from distutils.core import setup

setup(
    name='Py Bankkonto',
    version='0.0.3',
    long_description=open('README.rst').read(),
    install_requires=[],
    author='Albin Lindskog',
    author_email='albin@zerebra.com',
    url='https://https://bitbucket.org/AlbinLindskog/py_bankkonto',
    zip_safe=False,
    py_modules=['bankkonto'],
)