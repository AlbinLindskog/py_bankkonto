from re import match, sub


_banks = [{
        'name': 'Amfa Bank',
        'clearing_regex': r'^(966[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '966X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Avanza Bank',
        'clearing_regex': r'^(95[5-6][0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '95XX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'BlueStep Finans',
        'clearing_regex': r'^(968[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '968X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'BNP',
        'clearing_regex': r'^(947[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '947X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Citibank',
        'clearing_regex': r'^(904[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '904X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Danske Bank',
        'clearing_regex': r'^(1[2-3][0-9][0-9]|24[0-9][0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '1XXX, 24XX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Danske Bank',
        'clearing_regex': r'^(918[0-9])$',
        'banknr_regex': r'^([0-9]{10})$',
        'modulo': 10,
        'format': {
            'clearing_number': '918X',
            'bank_number': 'XXXX XXXX XX',
        },
        'lengths': {
            'clearing': 4,
            'account': 10,
            'control': 10,
        }
    }, {
        'name': 'DnB Bank',
        'clearing_regex': r'^(919[0-9]|926[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '919X, 926X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Ekobanken',
        'clearing_regex': r'^(970[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '970X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Erik Penser Bankaktiebolag',
        'clearing_regex': r'^(959[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '959X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Forex Bank',
        'clearing_regex': r'^(94[0-4][0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '94XX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Handelsbanken',
        'clearing_regex': r'^(6[0-9]{3})$',
        'banknr_regex': r'^([0-9]{9})$',
        'modulo': 11,
        'format': {
            'clearing_number': '6XXX',
            'bank_number': 'XXXX XXXX X',
        },
        'lengths': {
            'clearing': 4,
            'account': 9,
            'control': 9,
        }
    }, {
        'name': 'ICA Banken',
        'clearing_regex': r'^(927[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '927X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'IKANO Banken',
        'clearing_regex': r'^(917[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '917X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'JAK Medlemsbank',
        'clearing_regex': r'^(967[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '967X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Landshypotek',
        'clearing_regex': r'^(939[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '939X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Lån och Spar Bank Sverige',
        'clearing_regex': r'^(963[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '963X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Länsförsäkringar Bank',
        'clearing_regex': r'^(340[0-9]|906[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '340X, 906X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Länsförsäkringar Bank',
        'clearing_regex': r'^(902[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '902X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Marginalen Bank',
        'clearing_regex': r'^(923[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '923X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Medmera Bank AB',
        'clearing_regex': r'^(965[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '965X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Nordax Bank',
        'clearing_regex': r'^(964[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '964X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Nordea',
        'clearing_regex': r'^(11[0-9]{2}|1[4-9][0-9]{2}|20[0-9]{2}'
                 '|30[0-9]{2}|330[1-9]|33[1-9][0-9]|34[1-9]'
                 '[0-9]|3[5-9][0-9]{2})$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '11XX, 1XXX, 20XX, 30XX, 330X, 33XX, 34XX, 3XXX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Nordea',
        'clearing_regex': r'^(4[0-9]{3})$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '4XXX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Nordea',
        'clearing_regex': r'^(3300|3782)$',
        'banknr_regex': r'^([0-9]{10})$',
        'modulo': 10,
        'format': {
            'clearing_number': '3300, 3782',
            'bank_number': 'XXXX XXXX XX',
        },
        'lengths': {
            'clearing': 4,
            'account': 10,
            'control': 10,
        }
    }, {
        'name': 'Nordea',
        'clearing_regex': r'^$',
        'banknr_regex': r'^([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|[1-2][0-9]|3[0-1])'
                 '([0-9]{4})$',
        'modulo': 10,
        'format': {
            'clearing_number': '',
            'bank_number': 'XXXX XXXX XX',
        },
        'lengths': {
            'clearing': 0,
            'account': 10,
            'control': 10,
        }
    }, {
        'name': 'Nordnet Bank',
        'clearing_regex': r'^(910[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '910X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Resurs Bank',
        'clearing_regex': r'^(928[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '928X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Riksgälden',
        'clearing_regex': r'^(989[0-9])$',
        'banknr_regex': r'^([0-9]{10})$',
        'modulo': 10,
        'format': {
            'clearing_number': '989X',
            'bank_number': 'XXXX XXXX XX',
        },
        'lengths': {
            'clearing': 4,
            'account': 10,
            'control': 10,
        }
    }, {
        'name': 'Royal Bank of Scotland',
        'clearing_regex': r'^(909[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '909X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Santander Consumer Bank',
        'clearing_regex': r'^(946[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '946X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'SBAB',
        'clearing_regex': r'^(925[0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '925X',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'SEB',
        'clearing_regex': r'^(5[0-9]{3}|912[0-4]|91[3-4][0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '5XXX, 912X, 91XX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Skandiabanken',
        'clearing_regex': r'^(91[5-6][0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '91XX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }, {
        'name': 'Sparbanken Syd',
        'clearing_regex': r'^(957[0-9])$',
        'banknr_regex': r'^([0-9]{10})$',
        'modulo': 10,
        'format': {
            'clearing_number': '957X',
            'bank_number': 'XXXX XXXX XX',
        },
        'lengths': {
            'clearing': 4,
            'account': 10,
            'control': 10,
        }
    }, {
        'name': 'Swedbank',
        'clearing_regex': r'^(7[0-9]{3})$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '7XXX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 10,
        }
    }, {
        'name': 'Swedbank',
        'clearing_regex': r'^(93[0-2][0-9])$',
        'banknr_regex': r'^([0-9]{10})$',
        'modulo': 10,
        'format': {
            'clearing_number': '93XX',
            'bank_number': 'XXXX XXXX XX',
        },
        'lengths': {
            'clearing': 4,
            'account': 10,
            'control': 10,
        }
    }, {
        'name': 'Swedbank',
        'clearing_regex': r'^(8[0-9]{4})$',
        'banknr_regex': r'^([0-9]{10})$',
        'modulo': 10,
        'format': {
            'clearing_number': '8XXX',
            'bank_number': 'XXXX XXXX XX',
        },
        'lengths': {
            'clearing': 5,
            'account': 10,
            'control': 10,
        }
    }, {
        'name': 'Ålandsbanken',
        'clearing_regex': r'^(23[0-9][0-9])$',
        'banknr_regex': r'^([0-9]{7})$',
        'modulo': 11,
        'format': {
            'clearing_number': '23XX',
            'bank_number': 'XXXX XXX',
        },
        'lengths': {
            'clearing': 4,
            'account': 7,
            'control': 11,
        }
    }]


def _mod_11(num):
    weights = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    value = sum([weights[i % 10] * int(c) for i, c in enumerate(num[::-1])])
    return (value % 11) == 0


def _mod_10(num):
    weights = [1, 2]
    values = [weights[i % 2] * int(c) for i, c in enumerate(num[::-1])]
    value = sum([(v - 9) if (v > 9) else v for v in values])
    return (value % 10) == 0


def _clean_bank_number(n):
    n = sub('[\s\-\.]', '', str(n))
    return n


def clean_bank_info(cn, an):
    cn = _clean_bank_number(cn)
    an = _clean_bank_number(an)
    return cn, an


def get_bank_name(clearing_number):
    """
    Takes a clearing number and returns a string containing relating bank name

    :param clearing_number: The clearing number
    :return: Bank name if match or else return False
    """

    clearing = _clean_bank_number(clearing_number)

    for bank in _banks:
        if match(bank['clearing_regex'], str(clearing)):
            return bank['name']

    return False


def list_clearing_from_name(bank_name):
    """
    Takes a bank name and return a list of strings containing relating clearing number formats

    :param bank_name: The clearing number
    :param clearing_list: list containing clearing formats
    :return: List of clearing formats
    """

    clearing_list = []

    for bank in _banks:
        if bank['name'] == bank_name:
            clearing_list.append(bank['format']['clearing_number'])
    return clearing_list


def list_bank_number_formats_from_name(bank_name):
    """
    Takes a bank name and returns relating bank number formats

    :param bank_name: The clearing number
    :param format_list: list containing bank formats
    :return: List of bank number formats
    """

    format_list = []

    for bank in _banks:
        if bank['name'] == bank_name:
            format_list.append(bank['format']['bank_number'])
    return format_list


def get_bank_number_format_from_clearing(clearing_number):
    """
    Takes a clearing number and return a bank number format

    :param clearing_number: The clearing number
    :param clearing: clean clearing number
    :param format_list: list containing bank formats
    :return: Bank number formats or else return False
    """

    clearing = _clean_bank_number(clearing_number)

    for bank in _banks:
        if match(bank['clearing_regex'], str(clearing)):
            return bank['format']['bank_number']

    return False


def validate_bank_info(clearing_number, account_number, info=False):
    """
    Cleans and validates a clearing and account number

    :param clearing_number: The clearing number
    :param account_number: The account number
    :param info: Set to true to return the cleaned info as a dict instead of
    True if the information is valid.
    :return:
    """

    clearing_number, account_number = clean_bank_info(clearing_number,
                                                      account_number)
    bank_info = clearing_number + account_number

    if not match(r'^[\d]*$', bank_info):
        return False

    for bank in _banks:
        check_len = bank['lengths']['control']
        bank_account = bank_info[-check_len:]

        if match(bank['clearing_regex'], clearing_number) and \
        match(bank['banknr_regex'], account_number) and \
        ((bank['modulo'] == 11 and _mod_11(bank_account)) or
        (bank['modulo'] == 10 and _mod_10(bank_account))):

            if info:
                return {
                    'bank': bank['name'],
                    'clearing_number': clearing_number,
                    'account_number': account_number,
                }
            else:
                return True

    return False