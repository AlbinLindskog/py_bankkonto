A small library to handle swedish bank account information.
Based on the information provided by `bankgirot <https://www.bankgirot.se/globalassets/dokument/anvandarmanualer/bankernaskontonummeruppbyggnad_anvandarmanual_sv.pdf>`_.

USAGE
=====
Install the package with pip::

    pip3 install git+https://bitbucket.org/AlbinLindskog/py_bankkonto

import the validate_bank_info method::

    example.py
    from bankkonto import validate_bank_info

    clearing_number = 9420
    account_number = 4172385
    validate_bank_info(clearing_number, account_number)

SUPPORT
=======
Supports the following banks:

- Amfa Bank,
- Avanza Bank,
- BlueStep Finans,
- BNP,
- Citibank,
- Danske Bank,
- DnB Bank,
- Ekobanken,
- Erik Penser Bankaktiebolag,
- Forex Bank,
- Handelsbanken,
- ICA Banken,
- IKANO Banken,
- JAK Medlemsbank,
- Landshypotek,
- Lån och Spar Bank Sverige,
- Länsförsäkringar Bank,
- Marginalen Bank,
- Medmera Bank AB,
- Nordax Bank,
- Nordea,
- Nordnet Bank,
- Resurs Bank,
- Riksgälden,
- Royal Bank of Scotland,
- Santander Consumer Bank,
- SBAB,
- SEB,
- Skandiabanken,
- Sparbanken Syd,
- Swedbank*,
- Ålandsbanken


*in rare occasions some of Swedbank’s accounts cannot be validated by
a checksum calculation. In these cases this function will return False, despite
that fact these accounts are valid.

TODO
====
Add additional helper methods:

- Bank from clearing number
- Clearing number format from bank
- Account number format from clearing number
- Account number format from bank
- Add raise exception flag to all methods.