from unittest import TestCase, main

from bankkonto import _mod_10, _mod_11, validate_bank_info, clean_bank_info, get_bank_name, list_clearing_from_name, list_bank_number_formats_from_name, get_bank_number_format_from_clearing


class BankNameFromClearing(TestCase):
    """
    tests for clearing number as input and return bank name or False
    """

    def test_get_bank_name_valid_data(self):

        bank_name = get_bank_name('2300')
        self.assertEqual(bank_name, 'Ålandsbanken')

    def test_get_false_valid_data(self):
        """
        test if return false if clearing cant be found
        """
        bank_name = get_bank_name('9888')
        self.assertEqual(bank_name, False)

    def test_get_false_invalid_data(self):
        """
        test if return false if input has invalid data
        """
        bank_name = get_bank_name('invalid data')
        self.assertEqual(bank_name, False)


class ClearingFormat(TestCase):
    """
    tests for Bank name as input and return list of clearing formats or False
    """

    def test_get_clearing(self):
        """
        test bank name as input with list of clearing numbers as return value
        """
        clearing = list_clearing_from_name("Danske Bank")
        self.assertCountEqual(clearing, ['1XXX, 24XX', '918X'])

    def test_clearing_false(self):
        """
        test non-existing bank name as input and return empty list
        """
        clearing = list_clearing_from_name("Iron Bank")
        self.assertCountEqual(clearing, [])


class AccountFormatBankName(TestCase):
    """
    tests for Bank name as input and return list of account formats or False
    """

    def test_get_bank_form_name(self):
        """
        test bank name as input with list of bank number formats as return value
        """

        formats = list_bank_number_formats_from_name("Nordea")
        self.assertEqual(formats, ['XXXX XXX', 'XXXX XXX', 'XXXX XXXX XX', 'XXXX XXXX XX'])

    def test_format_false(self):
        """
        test bank name as input and return empty list
        """

        formats = list_bank_number_formats_from_name("Iron Bank")
        self.assertEqual(formats, [])


class AccountFormatClearing(TestCase):
    """
    tests for clearing as input and return account format or False
    """

    def test_get_bank_form_clearing(self):
        """
        test clearing as input with list of bank number formats as return value
        """

        account_format = get_bank_number_format_from_clearing('9895')
        self.assertEqual(account_format, 'XXXX XXXX XX')

    def test_get_false_valid_data(self):
        """
        test if return false if clearing cant be found
        """
        bank_name = get_bank_name('9888')
        self.assertEqual(bank_name, False)

    def test_get_false_invalid_data(self):
        """
        test if return false if input has invalid data
        """
        bank_name = get_bank_name('invalid data')
        self.assertEqual(bank_name, False)



class ValidateBankInfoTestCase(TestCase):
    """
    Tests for bankkonto.py.
    """

    def test_validate_correct(self):
        cn, an = '6600', '612 348 822'  # my account. plz sen monies
        self.assertTrue(validate_bank_info(cn, an))

    def test_validate_incorrect(self):
        cn, an = '6600', '612 348 823'
        self.assertFalse(validate_bank_info(cn, an))

    def test_info_flag(self):
        """
        The cleaned data shoudl be returned as a dict when the info 
        flag is True. 
        """
        cn, an = '6600', '612 348 822'
        info = validate_bank_info(cn, an, info=True)
        expected = {'bank': 'Handelsbanken',
                    'clearing_number': '6600',
                    'account_number': '612348822'}
        self.assertCountEqual(info, expected)

    def test_None(self):
        """
        Should be able to handle missing values.
        """
        cn, an = '6600', None
        res = validate_bank_info(cn, an)
        self.assertFalse(res)


class ModTestCase(TestCase):
    """
    Unittests for the helper functions _mod_10 and _mod_11.
    """

    def test_mod_10(self):
        num = "3316812057492"
        self.assertTrue(_mod_10(num))

    def test_mod_10_false(self):
        num = "3316812157492"
        self.assertFalse(_mod_10(num))

    def test_mod_11(self):
        num = '1912763608957'
        self.assertTrue(_mod_11(num))

    def test_mod_11_false(self):
        num = '1912763609957'
        self.assertFalse(_mod_11(num))


class CleanTestCase(TestCase):
    """
    Unittests for the clean functions.
    """

    def test_clean(self):
        """
        Clean should remove spaces and -.
        """
        cn, an = '66-00', '612.348 822'
        res = clean_bank_info(cn, an)
        expected = ('6600', '612348822')
        self.assertEqual(res, expected)

if __name__ == '__main__':
    main()